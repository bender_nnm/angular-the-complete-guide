import { Angular4TheCompleteGuideTheBasicsAppPage } from './app.po';

describe('angular4-the-complete-guide-the-basics-app App', () => {
  let page: Angular4TheCompleteGuideTheBasicsAppPage;

  beforeEach(() => {
    page = new Angular4TheCompleteGuideTheBasicsAppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
