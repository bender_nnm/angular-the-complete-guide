import { Action } from '@ngrx/store';

import { CONST } from '../../const';

export class SigninAction implements Action {
    readonly type: string = CONST.SIGN_IN;
    public payload: any;

    constructor() {
    }
}
