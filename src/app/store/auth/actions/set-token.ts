import { Action } from '@ngrx/store';

import { CONST } from '../../const';

export class SetTokenAction implements Action {
    readonly type: string = CONST.SET_TOKEN;

    constructor(public payload: string) {
    }
}
