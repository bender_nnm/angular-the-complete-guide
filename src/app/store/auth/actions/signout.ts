import { Action } from '@ngrx/store';

import { CONST } from '../../const';

export class SignoutAction implements Action {
    readonly type: string = CONST.SIGN_OUT;
    public payload: any;

    constructor() {
    }
}
