import { SignupAction } from './signup';
import { SigninAction } from './signin';
import { SignoutAction } from './signout';
import { SetTokenAction } from './set-token';

export { SignupAction } from './signup';
export { SigninAction } from './signin';
export { SignoutAction } from './signout';
export { SetTokenAction } from './set-token';

export const AuthAction = {
    SignupAction,
    SigninAction,
    SignoutAction,
    SetTokenAction
};

export type AuthActions =
    SignupAction
    | SigninAction
    | SignoutAction
    | SetTokenAction;
