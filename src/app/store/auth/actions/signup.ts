import { Action } from '@ngrx/store';

import { CONST } from '../../const';

export class SignupAction implements Action {
    readonly type: string = CONST.SIGN_UP;
    public payload: any;

    constructor() {
    }
}
