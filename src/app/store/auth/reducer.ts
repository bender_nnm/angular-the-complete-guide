import  { CONST } from '../const';
import { AuthState } from './state';
import { AuthActions } from './actions';

const initialAuthState: AuthState = {
    token        : null,
    authenticated: false
};

export function authReducer(state = initialAuthState, action: AuthActions) {
    const payload = action.payload;

    switch (action.type) {

        case CONST.SIGN_UP:
        case CONST.SIGN_IN: {
            return {
                ...state,
                authenticated: true
            };
        }

        case CONST.SIGN_OUT: {
            return {
                ...state,
                token        : null,
                authenticated: false
            };
        }

        case CONST.SET_TOKEN: {
            return {...state, token: payload};
        }

    }

    return state;
}
