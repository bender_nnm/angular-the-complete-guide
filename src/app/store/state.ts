import { AuthState } from './auth';
import { ShoppingListState } from './shopping-list';

export interface  AppState {
    auth: AuthState;
    shoppingList: ShoppingListState;
}
