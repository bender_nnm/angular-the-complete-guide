import { CONST } from '../const'
import { IngredientModel } from '../../shared/models';
import { ShoppingListState } from './state';
import { ShoppingListActions } from './actions';

const initialShoppingListState: ShoppingListState = {
    editIngredientIndex: -1,
    editIngredient     : null,
    ingredients        : [
        new IngredientModel('Bred', 1),
        new IngredientModel('Apples', 5),
        new IngredientModel('Tomatos', 10)
    ]
};

export function shoppingListReducer(state = initialShoppingListState, action: ShoppingListActions) {
    const payload = action.payload;

    switch (action.type) {

        case CONST.ADD_INGREDIENT: {
            return {
                ...state,
                ingredients: [...state.ingredients, payload]
            };
        }

        case CONST.ADD_INGREDIENTS: {
            return {
                ...state,
                ingredients: [...state.ingredients, ...payload]
            };
        }

        case CONST.UPDATE_INGREDIENT: {
            const ingredient = state.ingredients[state.editIngredientIndex];
            const updatedIngredient = {
                ...ingredient,
                ...payload
            };
            const ingredients = [...state.ingredients];

            ingredients[state.editIngredientIndex] = updatedIngredient;

            return {
                ...state,
                ingredients,
                editIngredientIndex: -1,
                editIngredient     : null
            };
        }

        case CONST.DELETE_INGREDIENT: {
            const ingredients = [...state.ingredients];

            ingredients.splice(state.editIngredientIndex, 1);

            return {
                ...state,
                ingredients,
                editIngredientIndex: -1,
                editIngredient     : null
            };
        }

        case CONST.START_UPDATE_INGREDIENT: {
            const editIngredientIndex = payload;
            const editIngredient = {...state.ingredients[editIngredientIndex]};

            return {
                ...state,
                editIngredient,
                editIngredientIndex
            };
        }

        case CONST.STOP_UPDATE_INGREDIENT: {
            return {
                ...state,
                editIngredientIndex: -1,
                editIngredient     : null
            };
        }
    }

    return state;
}
