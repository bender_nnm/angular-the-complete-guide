import { IngredientModel } from '../../shared';

export interface ShoppingListState {
    editIngredientIndex: number;
    ingredients: IngredientModel[];
    editIngredient: IngredientModel;
}
