import { AddIngredient } from './add-ingredient';
import { AddIngredients } from './add-ingredients';
import { DeleteIngredient } from './delete-ingredient';
import { UpdateIngredient } from './update-ingredient';
import { StopUpdateIngredient } from './stop-update-ingredient';
import { StartUpdateIngredient } from './start-update-ingredient';

export { AddIngredient } from './add-ingredient';
export { AddIngredients } from './add-ingredients';
export { DeleteIngredient } from './delete-ingredient';
export { UpdateIngredient } from './update-ingredient';
export { StopUpdateIngredient } from './stop-update-ingredient';
export { StartUpdateIngredient } from './start-update-ingredient';

export const ShoppingListAction = {
    AddIngredient,
    AddIngredients,
    DeleteIngredient,
    UpdateIngredient,
    StopUpdateIngredient,
    StartUpdateIngredient
};

export type ShoppingListActions =
    AddIngredient
    | AddIngredients
    | UpdateIngredient
    | DeleteIngredient
    | StopUpdateIngredient
    | StartUpdateIngredient;
