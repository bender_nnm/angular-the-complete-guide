import { Action } from  '@ngrx/store';

import { CONST } from '../../const';

export class StartUpdateIngredient implements Action {

    readonly type: string = CONST.START_UPDATE_INGREDIENT;

    constructor(public payload: number) {
    }

}
