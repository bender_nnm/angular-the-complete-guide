import { Action } from  '@ngrx/store';

import { CONST } from '../../const';

export class DeleteIngredient implements Action {

    public payload: any;
    readonly type: string = CONST.DELETE_INGREDIENT;

    constructor() {
    }

}
