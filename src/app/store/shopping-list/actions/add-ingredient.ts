import {Action} from '@ngrx/store';

import {CONST} from '../../const';
import {IngredientModel} from '../../../shared/models';

export class AddIngredient implements Action {

    readonly type: string = CONST.ADD_INGREDIENT;

    constructor(public payload: IngredientModel) {
    }

}
