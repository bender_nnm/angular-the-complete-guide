import { Action } from  '@ngrx/store';

import { CONST } from '../../const';
import { IngredientModel } from '../../../shared/models';

export class UpdateIngredient implements Action {

    readonly type: string = CONST.UPDATE_INGREDIENT;

    constructor(public payload: IngredientModel) {
    }

}
