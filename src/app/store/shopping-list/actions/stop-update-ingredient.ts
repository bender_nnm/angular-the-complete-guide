import {Action} from '@ngrx/store';

import {CONST} from '../../const';

export class StopUpdateIngredient implements Action {

    public payload: any;
    readonly type: string = CONST.STOP_UPDATE_INGREDIENT;

}
