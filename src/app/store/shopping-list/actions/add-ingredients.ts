import { Action } from  '@ngrx/store';

import { CONST } from '../../const';
import { IngredientModel } from '../../../shared/models';

export class AddIngredients implements Action {

    readonly type: string = CONST.ADD_INGREDIENTS;

    constructor(public payload: IngredientModel[]) {
    }

}
