import { ActionReducerMap } from '@ngrx/store';

import { AppState } from './state';
import { authReducer } from './auth';
import { shoppingListReducer } from './shopping-list';

export const reducers: ActionReducerMap<AppState> = {
    auth        : authReducer,
    shoppingList: shoppingListReducer
};