export const CONST = {
    ADD_INGREDIENT         : 'add ingredient',
    ADD_INGREDIENTS        : 'add ingredients',
    UPDATE_INGREDIENT      : 'update ingredient',
    DELETE_INGREDIENT      : 'delete ingredient',
    START_UPDATE_INGREDIENT: 'start update ingredient',
    STOP_UPDATE_INGREDIENT : 'stop update ingredient',
    SIGN_UP                : 'sign up',
    SIGN_IN                : 'sign in',
    SIGN_OUT               : 'sign out',
    SET_TOKEN              : 'set token'
};
