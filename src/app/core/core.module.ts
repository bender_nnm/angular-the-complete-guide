import { NgModule } from '@angular/core';

import { AppRoutingModule } from '../app-routing.module';

import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './header/header.component';

import { ShareModule, AuthGuard, RecipeService,  AuthService } from '../shared';

@NgModule({
  declarations: [
    HomeComponent,
    HeaderComponent
  ],
  imports     : [
    ShareModule,
    AppRoutingModule
  ],
  exports     : [
    HomeComponent,
    HeaderComponent,
    AppRoutingModule
  ],
  providers   : [
    AuthGuard,
    AuthService,
    RecipeService
  ]
})
export class CoreModule {
}
