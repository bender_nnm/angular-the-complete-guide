import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { AppState, AuthState } from '../../store';
import { AuthService, RecipeService } from '../../shared';

@Component({
    selector   : 'app-header',
    templateUrl: './header.component.html'
})
export class HeaderComponent implements OnInit {

    authState: Observable<AuthState>;

    constructor(private store: Store<AppState>,
                private authService: AuthService,
                private recipeService: RecipeService) {
    }

    ngOnInit(): void {
        this.authState = this.store.select('auth');
    }

    onSave(): void {
        this.recipeService.saveRecipes()
            .subscribe(
                (xhr) => console.log(xhr)
            );
    }

    onFetch(): void {
        this.recipeService.getRecipes()
            .subscribe(
                (recipes) => {
                    this.recipeService.recipes = recipes;
                }
            );
    }

    signOut() {
        this.authService.signOut();
    }

}
