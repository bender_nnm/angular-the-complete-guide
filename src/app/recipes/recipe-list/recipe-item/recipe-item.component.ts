import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

import { RecipeModel } from '../../../shared';
import { RecipeService } from "../../../shared/services/recipe.service";

@Component({
  selector   : 'app-recipe-item',
  templateUrl: './recipe-item.component.html',
  styleUrls  : ['./recipe-item.component.css']
})
export class RecipeItemComponent implements OnInit {

  @Input()
  private index: number;

  @Input()
  private recipe: RecipeModel;

  constructor(private recipeService: RecipeService) {
  }

  ngOnInit(): void {
  }

}
