import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";

import { RecipeModel, RecipeService } from '../../shared';

@Component({
  selector   : 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls  : ['./recipe-list.component.css']
})
export class RecipeListComponent implements OnInit {

  private recipes: RecipeModel[];

  constructor(private router: Router,
              private route: ActivatedRoute,
              private recipeService: RecipeService) {
  }

  ngOnInit(): void {
    this.recipes = this.recipeService.recipes;
  }

  createNewRecipe(): void {
    this.router.navigate(['new'], {relativeTo: this.route});
  }

}
