import { FormGroup, FormControl, FormArray, Validators } from '@angular/forms';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from "@angular/router";

import { RecipeModel, RecipeService } from "../../shared";

@Component({
  selector   : 'app-recipe-edit',
  templateUrl: './recipe-edit.component.html',
  styleUrls  : ['./recipe-edit.component.css']
})
export class RecipeEditComponent implements OnInit {

  private id: number;
  private editMode: boolean = false;
  private amountPattern = /^[1-9]+[0-9]*$/;
  private form: FormGroup;

  constructor(private router: Router,
              private route: ActivatedRoute,
              private recipeService: RecipeService) {
  }

  ngOnInit(): void {
    this.route.params
      .subscribe(
        (params: Params) => {
          this.id = +params['id'];
          this.editMode = params['id'] != null;

          this.initForm();
        }
      );
  }

  private initForm(): void {
    let recipeName = '';
    let recipeImagePath = '';
    let recipeDescription = '';
    let recipeIngredients = new FormArray([]);

    if (this.editMode) {
      const recipe = this.recipeService.getRecipe(this.id);

      recipeName = recipe.name;
      recipeImagePath = recipe.imagePath;
      recipeDescription = recipe.description;

      if (recipe.ingredients) {
        for (let ingredient of recipe.ingredients) {
          recipeIngredients.push(
            new FormGroup({
              'name'  : new FormControl(ingredient.name, Validators.required),
              'amount': new FormControl(ingredient.amount, [
                Validators.required,
                Validators.pattern(this.amountPattern)
              ])
            })
          );
        }
      }
    }

    this.form = new FormGroup({
      'name'       : new FormControl(recipeName, Validators.required),
      'imagePath'  : new FormControl(recipeImagePath, Validators.required),
      'description': new FormControl(recipeDescription, Validators.required),
      'ingredients': recipeIngredients
    });
  }

  onAddIngredient(): void {
    (<FormArray>this.form.get('ingredients')).push(
      new FormGroup({
        'name'  : new FormControl('ingredient', Validators.required),
        'amount': new FormControl(1, [
          Validators.required,
          Validators.pattern(this.amountPattern)
        ])
      })
    );
  }

  onDeleteIngredient(index: number): void {
    (<FormArray>this.form.get('ingredients')).removeAt(index);
  }

  onSubmit(): void {
    const value = this.form.value;
    const name = value.name;
    const imagePath = value.imagePath;
    const description = value.description;
    const ingredients = value.ingredients;
    const newRecipe = new RecipeModel(name, imagePath, description, ingredients);

    if (this.editMode) {
      this.recipeService.updateRecipe(this.id, newRecipe);
    } else {
      this.recipeService.addRecipe(newRecipe);
    }

    this.router.navigate(['/recipes']);
  }

  onCancel(): void {
    const cancel = confirm(`Don\'t ${ this.editMode ? 'edit' : 'save' } it?`);

    cancel && this.router.navigate(['/recipes']);
  }

}
