import { Store } from  '@ngrx/store';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';

import { AppState, AddIngredients } from '../../store';
import { RecipeModel, RecipeService } from '../../shared';

@Component({
    selector   : 'app-recipe-detail',
    templateUrl: './recipe-detail.component.html',
    styleUrls  : ['./recipe-detail.component.css']
})
export class RecipeDetailComponent implements OnInit {

    private id: number;
    private recipe: RecipeModel;

    constructor(private router: Router,
                private route: ActivatedRoute,
                private store: Store<AppState>,
                private recipeService: RecipeService) {
    }

    ngOnInit(): void {
        this.route.params.subscribe(
            (params: Params) => {
                this.id = +params['id'];
                this.recipe = this.recipeService.getRecipe(this.id);
            }
        );
    }

    onDelete() {
        this.recipeService.deleteRecipe(this.id);
        this.router.navigate(['/recipes']);
    }

    onToShoppingList() {
        this.store.dispatch(new AddIngredients(this.recipe.ingredients));
    }

}
