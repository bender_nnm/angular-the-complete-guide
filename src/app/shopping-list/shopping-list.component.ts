import { Store } from '@ngrx/store';
import { Component, OnInit } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { AppState, StartUpdateIngredient } from '../store';
import { IngredientModel } from '../shared';

@Component({
    selector   : 'app-shopping-list',
    templateUrl: './shopping-list.component.html',
    styleUrls  : ['./shopping-list.component.css']
})
export class ShoppingListComponent implements OnInit {

    private shoppingListState: Observable<{ ingredients: IngredientModel[] }>;

    constructor(private store: Store<AppState>) {
    }

    ngOnInit(): void {
        this.shoppingListState = this.store.select('shoppingList');
    }

    onEditItem(index: number) {
        this.store.dispatch(new StartUpdateIngredient(index));
    }

}
