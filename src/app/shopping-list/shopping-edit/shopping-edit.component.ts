import {Store} from '@ngrx/store';
import {NgForm} from '@angular/forms';
import {Subscription} from 'rxjs/Subscription';
import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';

import {AddIngredient, AppState, DeleteIngredient, StopUpdateIngredient, UpdateIngredient} from '../../store';

import {IngredientModel} from '../../shared';

@Component({
    selector   : 'app-shopping-edit',
    templateUrl: './shopping-edit.component.html',
    styleUrls  : ['./shopping-edit.component.css']
})
export class ShoppingEditComponent implements OnInit, OnDestroy {

    @ViewChild('form')
    private form: NgForm;

    private editMode = false;
    private subscriptionOnShoppingList: Subscription;

    constructor(private store: Store<AppState>) {
    }

    ngOnInit(): void {
        this.subscriptionOnShoppingList = this.store.select('shoppingList').subscribe(
            data => {
                if (data.editIngredientIndex > -1) {
                    const editItem = data.editIngredient;

                    this.editMode = true;
                    this.form.setValue(editItem);
                }
            }
        );
    }

    ngOnDestroy(): void {
        this.subscriptionOnShoppingList.unsubscribe();
        this.store.dispatch(new StopUpdateIngredient());
    }

    onClickItem() {
        if (this.editMode) {
            return this.onEditItem();
        }

        this.onAddNewItem();
    }

    onAddNewItem() {
        const value: any = this.form.value;
        const newIngredient: IngredientModel = new IngredientModel(value.name, value.amount);

        this.store.dispatch(new AddIngredient(newIngredient));

        this.form.reset();
    }

    onEditItem() {
        const value: any = this.form.value;
        const ingredient = new IngredientModel(value.name, value.amount);

        this.store.dispatch(new UpdateIngredient(ingredient));
        this.form.reset();
        this.editMode = false;
    }

    onDelete() {
        this.store.dispatch(new DeleteIngredient());
        this.form.reset();
        this.editMode = false;
    }

    onClear() {
        this.form.reset();
        this.editMode = false;
    }
}
