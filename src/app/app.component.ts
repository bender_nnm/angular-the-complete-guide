import { Component, OnInit } from '@angular/core';
import * as firebase from 'firebase';

@Component({
  selector   : 'app-root',
  templateUrl: './app.component.html',
  styleUrls  : ['./app.component.css']
})
export class AppComponent implements OnInit {

  ngOnInit(): void {
    firebase.initializeApp({
      authDomain: 'ng-recipe-book-f1ed4.firebaseapp.com',
      apiKey    : 'AIzaSyA9CnL-PTsYRRqStKs6VCInClS_Ztf0-xA'
    })
  };

}
