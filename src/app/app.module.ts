import { NgModule } from '@angular/core';
import { StoreModule } from '@ngrx/store';
import { HttpModule } from '@angular/http';
import { BrowserModule } from '@angular/platform-browser';

import 'rxjs/Rx';

import { CoreModule } from './core/core.module';
import { AuthModule } from './auth/auth.module';
import { ShoppingListModule } from './shopping-list/shopping-list.module';

import { AppComponent } from './app.component';

import { reducers } from './store';
import { ShareModule } from './shared';

@NgModule({
    declarations: [
        AppComponent
    ],
    imports     : [
        AuthModule,
        HttpModule,
        ShareModule,
        BrowserModule,
        ShoppingListModule,
        CoreModule,
        StoreModule.forRoot(reducers)
    ],
    providers   : [],
    bootstrap   : [AppComponent]
})
export class AppModule {
}
