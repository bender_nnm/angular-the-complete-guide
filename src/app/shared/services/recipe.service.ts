import { Store } from '@ngrx/store';
import { Http } from '@angular/http';
import { Injectable } from '@angular/core';

import { Observable } from 'rxjs/Observable';

import { AuthService } from './auth.service';
import { RecipeModel, IngredientModel } from '../models';
import { CONST } from '../const';

import { AppState, AuthState } from '../../store';

@Injectable()
export class RecipeService {

    private _recipes: Array<RecipeModel> = [
        new RecipeModel(
            'Tasty Schnitzel',
            'http://givememora.com/wp-content/uploads/2014/03/shnitzel.jpg',
            'A super-tasty Schnitzel - just awesome!',
            [
                new IngredientModel('Meat', 1),
                new IngredientModel('French Fries', 20)
            ]),
        new RecipeModel('Big Fat Burger',
            'http://static.upcoming.nl/static/images/68e9590663_1375709894_Bungalow-8-burger__list-noup.jpg',
            'What else you need to say?',
            [
                new IngredientModel('Buns', 2),
                new IngredientModel('Meat', 1)
            ])
    ];

    constructor(private http: Http,
                private store: Store<AppState>,
                private authService: AuthService) {
    }

    get recipes(): RecipeModel[] {
        return this._recipes;
    }

    set recipes(recipes: RecipeModel[]) {
        const length = this._recipes.length;
        this._recipes.splice(0, length, ...recipes);
    }

    getRecipes(): Observable<any> {
        return this.store.select('auth')
            .take(1)
            .switchMap(
                (authState: AuthState) => {
                    return this.http.get(CONST.fetchUrl + authState.token)
                        .map(xhr => xhr.json());
                }
            );
    }

    saveRecipes(): Observable<any> {
        return this.http.put('https://ng-recipe-book-f1ed4.firebaseio.com/recipes.json', this._recipes);
    }

    getRecipe(id): RecipeModel {
        return this._recipes[id];
    }

    addRecipe(recipe: RecipeModel): void {
        this._recipes.push(recipe);
    }

    updateRecipe(index: number, newRecipe: RecipeModel): void {
        this._recipes[index] = newRecipe;
    }

    deleteRecipe(index: number): void {
        this._recipes.splice(index, 1);
    }

}
