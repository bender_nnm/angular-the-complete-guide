import { Store } from '@ngrx/store';
import { Router } from '@angular/router';
import { Injectable } from '@angular/core';

import * as firebase from 'firebase';

import { AppState, SignupAction, SigninAction, SignoutAction, SetTokenAction } from '../../store';

@Injectable()
export class AuthService {

    constructor(private store: Store<AppState>,
                private router: Router) {
    }

    signUp(email: string, password: string) {
        firebase.auth().createUserWithEmailAndPassword(email, password)
            .then(
                user => {
                    this.store.dispatch(new SignupAction());

                    firebase
                        .auth()
                        .currentUser
                        .getToken()
                        .then(
                            (token: string) => this.store.dispatch(new SetTokenAction(token))
                        );
                }
            )
            .catch(
                err => console.log(err)
            )
    }

    signIn(email: string, password: string) {
        firebase.auth().signInWithEmailAndPassword(email, password)
            .then(
                res => {
                    this.store.dispatch(new SigninAction());
                    this.router.navigate(['/']);

                    firebase
                        .auth()
                        .currentUser
                        .getToken()
                        .then(
                            (token: string) => this.store.dispatch(new SetTokenAction(token))
                        );
                }
            )
            .catch(
                err => console.log(err)
            );
    }

    signOut() {
        this.store.dispatch(new SignoutAction());
        firebase.auth().signOut();
    }

}

