import { IngredientModel } from './ingredient.model';

export class RecipeModel {

  constructor(public name: string,
              public imagePath: string,
              public description: string,
              public ingredients: IngredientModel[]) {
  }

}
