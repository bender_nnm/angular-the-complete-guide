import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DropDownDirective } from './directives';

@NgModule({
  declarations: [
    DropDownDirective
  ],
  exports     : [
    CommonModule,
    DropDownDirective
  ]
})
export class ShareModule {
}
