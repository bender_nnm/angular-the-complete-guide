import {Store} from '@ngrx/store';
import {Injectable} from '@angular/core';
import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot} from '@angular/router';


import {Observable} from 'rxjs/Observable';

import {AppState, AuthState} from '../../store';

@Injectable()
export class AuthGuard implements CanActivate {

    constructor(private store: Store<AppState>) {
    }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot):
        Observable<boolean> | Promise<boolean> | boolean {
        return this.store.select('auth').map((authState: AuthState) => authState.authenticated);
    }

}
